-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           5.7.33 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Listage de la structure de la base pour fox
DROP DATABASE IF EXISTS `fox`;
CREATE DATABASE IF NOT EXISTS `fox` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fox`;

-- Listage de la structure de la table fox. phinxlog
DROP TABLE IF EXISTS `phinxlog`;
CREATE TABLE IF NOT EXISTS `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table fox.phinxlog : ~2 rows (environ)
DELETE FROM `phinxlog`;
/*!40000 ALTER TABLE `phinxlog` DISABLE KEYS */;
INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
	(20221114125254, 'UsersMigration', '2022-12-01 12:37:51', '2022-12-01 12:37:51', 0),
	(20221120071435, 'ProductsMigration', '2022-12-01 12:37:51', '2022-12-01 12:37:51', 0);
/*!40000 ALTER TABLE `phinxlog` ENABLE KEYS */;

-- Listage de la structure de la table fox. products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_activated` tinyint(1) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `delivery_delay` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table fox.products : ~100 rows (environ)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `origin`, `is_activated`, `image`, `description`, `delivery_delay`, `price`, `created_at`, `updated_at`) VALUES
	(1, 'Marocain Rouge', '@Marocco', 1, './../resources/assets/img/marocain_rouge.webp', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Natus quas consequatur aut sequi facere maiores repellat at aspernatur atque necessitatibus tenetur voluptatum ipsa hic, nemo modi similique tempora, temporibus soluta, consequuntur iure? Asperiores corporis minus delectus laudantium accusamus dignissimos aliquam suscipit blanditiis distinctio placeat eveniet praesentium sequi consectetur, harum, dolores dolore iusto, nulla autem id mollitia numquam. Modi minus vitae explicabo neque. Illum libero eos odit modi iure, sunt accusamus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris. @bulmaio. #css #responsive.', 21, 25, '2022-12-01 12:37:51', NULL),
	(2, 'Cannabis', '@Asian', 1, './../resources/assets/img/cannabis.webp', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid, incidunt vel! Consequuntur, unde dolorum est voluptatibus assumenda sunt tempore perferendis sequi officia ut officiis quis deserunt quisquam eius. Voluptatem, odit. Form cannabis Sativa.', 7, 20, '2022-12-01 12:37:51', NULL),
	(3, 'CBD', '@France', 1, './../resources/assets/img/cbd.webp', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni qui odio, quis exercitationem voluptas quidem voluptatem delectus sint obcaecati deserunt, nesciunt eveniet tenetur dolorum ab quibusdam natus adipisci assumenda. Distinctio illum veniam ea error id odit neque harum voluptate nulla voluptatum? Maxime eaque nulla quidem doloremque, dolorem magni? Obcaecati, exercitationem.', 3, 50, '2022-12-01 12:37:51', NULL),
	(4, 'Voluptas Reiciendis', '@Slovakia (Slovak Republic)', 1, 'https://picsum.photos/140/100?random=0', 'Id quia voluptas culpa est officiis quam. Quia dolorum ea saepe dolor magni.', 7, 90, '2022-12-01 12:37:52', NULL),
	(5, 'Autem', '@New Zealand', 1, 'https://picsum.photos/140/100?random=1', 'Cupiditate omnis nesciunt repellendus dignissimos. Perspiciatis cum voluptas error rerum harum architecto nihil.', 14, 97, '2022-12-01 12:37:52', NULL),
	(6, 'Quae Aperiam', '@Syrian Arab Republic', 1, 'https://picsum.photos/140/100?random=2', 'Illo officia reprehenderit quis. Qui ipsa voluptatem et ab. Possimus totam id quia sit fuga quia.', 7, 23, '2022-12-01 12:37:52', NULL),
	(7, 'Dolorem Est', '@Tajikistan', NULL, 'https://picsum.photos/140/100?random=3', 'Necessitatibus eius sunt dolores cumque quisquam fuga. Laudantium perferendis necessitatibus praesentium delectus.', 3, 18, '2022-12-01 12:37:52', NULL),
	(8, 'Eveniet Sint', '@Kuwait', NULL, 'https://picsum.photos/140/100?random=4', 'Adipisci est alias et assumenda voluptatem accusantium. Qui incidunt accusamus dolorem qui est libero provident.', 30, 4, '2022-12-01 12:37:52', NULL),
	(9, 'Illum Assumenda', '@Botswana', 1, 'https://picsum.photos/140/100?random=5', 'Assumenda dicta vero enim quia. Et aut minima commodi perferendis.', 14, 65, '2022-12-01 12:37:52', NULL),
	(10, 'Adipisci Sequi', '@Norway', 1, 'https://picsum.photos/140/100?random=6', 'Tenetur quam minus occaecati repudiandae. Eligendi eum asperiores et distinctio quos reprehenderit. Est exercitationem consequatur vel error perferendis ipsam sapiente et.', 21, 63, '2022-12-01 12:37:52', NULL),
	(11, 'Sit Occaecati', '@Turkmenistan', 1, 'https://picsum.photos/140/100?random=7', 'Et quis ab voluptatem sequi sit modi porro. Iste quo dolore dolorem tempore ullam unde. Itaque dolorem esse architecto quis quis et.', 7, 45, '2022-12-01 12:37:52', NULL),
	(12, 'Nesciunt Quam', '@El Salvador', 1, 'https://picsum.photos/140/100?random=8', 'Voluptatum corrupti delectus iste nam et exercitationem. Ut ea iste voluptatem eum corrupti cumque natus ut.', 21, 35, '2022-12-01 12:37:52', NULL),
	(13, 'Et', '@New Caledonia', 1, 'https://picsum.photos/140/100?random=9', 'Quo sit sed molestiae accusamus. Corrupti quia et voluptatem distinctio fugit.', 14, 20, '2022-12-01 12:37:52', NULL),
	(14, 'Adipisci Nostrum', '@Liechtenstein', 1, 'https://picsum.photos/140/100?random=10', 'Velit nam voluptatibus voluptatem voluptate doloremque consequatur. Quia commodi et doloremque iure.', 14, 76, '2022-12-01 12:37:52', NULL),
	(15, 'Animi Adipisci', '@Turkmenistan', 1, 'https://picsum.photos/140/100?random=11', 'Eligendi ut nulla minima non ut. Quis sint exercitationem inventore sit amet.', 14, 86, '2022-12-01 12:37:52', NULL),
	(16, 'Consequatur', '@Vanuatu', 1, 'https://picsum.photos/140/100?random=12', 'Iste doloribus blanditiis consequatur ipsa. Quia ut commodi aut perspiciatis nam rem ea repellendus. Et sit amet consectetur itaque quis.', 5, 23, '2022-12-01 12:37:52', NULL),
	(17, 'Necessitatibus', '@Ireland', 1, 'https://picsum.photos/140/100?random=13', 'Omnis rem ut fuga aliquid. Repellat voluptatem adipisci natus ducimus.', 5, 14, '2022-12-01 12:37:52', NULL),
	(18, 'Sint', '@Mongolia', 1, 'https://picsum.photos/140/100?random=14', 'Nulla mollitia non quo nostrum necessitatibus ea. Et temporibus et sit et eos ut et. Et aut repellat possimus amet quia modi quod.', 14, 12, '2022-12-01 12:37:52', NULL),
	(19, 'Excepturi', '@Cambodia', 1, 'https://picsum.photos/140/100?random=15', 'Molestias non deserunt in nemo nobis. Cumque rerum mollitia in nam nam nobis molestias.', 7, 28, '2022-12-01 12:37:52', NULL),
	(20, 'Natus', '@United States of America', 1, 'https://picsum.photos/140/100?random=16', 'Quam laudantium ut placeat. Qui minus qui eos. Quod aut id consequuntur quas.', 7, 96, '2022-12-01 12:37:52', NULL),
	(21, 'Voluptatum', '@United States of America', 1, 'https://picsum.photos/140/100?random=17', 'Rerum sunt aspernatur earum aut laboriosam enim dolores corporis. Perferendis repellat dolorum et accusantium maiores qui asperiores. Perferendis qui porro fugit eaque aliquid sed tempora.', 3, 79, '2022-12-01 12:37:52', NULL),
	(22, 'Libero', '@Croatia', 1, 'https://picsum.photos/140/100?random=18', 'Dolorem consequuntur sit aut sit nostrum eaque. Officia harum similique et qui eaque nostrum. Vel natus nihil est.', 5, 91, '2022-12-01 12:37:52', NULL),
	(23, 'Impedit Explicabo', '@Niue', 1, 'https://picsum.photos/140/100?random=19', 'Ullam dolor occaecati aut vero ut hic impedit. Ut nulla quasi incidunt consequuntur dolorem. Esse et odio et magni incidunt modi.', 7, 39, '2022-12-01 12:37:52', NULL),
	(24, 'Quidem Debitis', '@Turks and Caicos Islands', 1, 'https://picsum.photos/140/100?random=20', 'Totam illum qui suscipit non mollitia occaecati. Quos assumenda dolore omnis quia ullam aut.', 30, 4, '2022-12-01 12:37:52', NULL),
	(25, 'Assumenda', '@Angola', 1, 'https://picsum.photos/140/100?random=21', 'Doloribus eaque et ex adipisci dolorum est voluptas. Enim voluptates aliquam qui distinctio. Est corrupti nihil quo quae.', 3, 88, '2022-12-01 12:37:52', NULL),
	(26, 'Distinctio Odio', '@France', 1, 'https://picsum.photos/140/100?random=22', 'Nisi sit est doloribus voluptates aut. Quis dolorum quia id vero eos autem atque.', 5, 43, '2022-12-01 12:37:52', NULL),
	(27, 'Quo', '@Iran', 1, 'https://picsum.photos/140/100?random=23', 'Et debitis et et ut voluptatibus. Magnam non dolorum adipisci.', 3, 43, '2022-12-01 12:37:52', NULL),
	(28, 'Consectetur', '@Philippines', 1, 'https://picsum.photos/140/100?random=24', 'Ut quo exercitationem dicta possimus. Libero voluptatibus sapiente aut numquam accusantium omnis.', 3, 74, '2022-12-01 12:37:52', NULL),
	(29, 'Provident', '@Saint Pierre and Miquelon', 1, 'https://picsum.photos/140/100?random=25', 'In eius sit nemo beatae. Aliquid velit quo maiores sint perspiciatis.', 14, 21, '2022-12-01 12:37:52', NULL),
	(30, 'Repudiandae', '@Bouvet Island (Bouvetoya)', 1, 'https://picsum.photos/140/100?random=26', 'Repudiandae aliquid aut quia mollitia magni dolores non porro. Iusto minima iste laborum omnis. Inventore omnis id similique labore aut aut.', 3, 14, '2022-12-01 12:37:52', NULL),
	(31, 'Voluptatem', '@Taiwan', 1, 'https://picsum.photos/140/100?random=27', 'Repudiandae aut enim quia iusto assumenda eos assumenda. Optio quisquam natus officiis at perferendis deleniti. Reprehenderit nostrum fugit veritatis sit magni.', 14, 9, '2022-12-01 12:37:52', NULL),
	(32, 'Amet', '@Iran', NULL, 'https://picsum.photos/140/100?random=28', 'Et sapiente molestiae accusamus reiciendis ea. Ut repudiandae est ratione est accusamus inventore labore. Nostrum est similique aut occaecati odio consequuntur ut.', 14, 15, '2022-12-01 12:37:52', NULL),
	(33, 'Quos', '@Grenada', 1, 'https://picsum.photos/140/100?random=29', 'Ratione ut aperiam quo sed. Rerum eos incidunt vel provident id quibusdam omnis. Optio aut aspernatur blanditiis illum quia.', 3, 44, '2022-12-01 12:37:52', NULL),
	(34, 'Accusantium Nisi', '@Seychelles', 1, 'https://picsum.photos/140/100?random=30', 'Ipsam eligendi explicabo quia quia assumenda doloribus est. Enim autem sint consectetur vitae. Non ut vel autem consequuntur aut dolores rem.', 3, 5, '2022-12-01 12:37:52', NULL),
	(35, 'Voluptatum Vel', '@El Salvador', 1, 'https://picsum.photos/140/100?random=31', 'Accusantium itaque autem doloremque eum commodi. Dolore ut corrupti deserunt dolor. Aperiam vel veniam explicabo eaque esse et ut suscipit.', 30, 64, '2022-12-01 12:37:52', NULL),
	(36, 'Optio Laboriosam', '@Comoros', 1, 'https://picsum.photos/140/100?random=32', 'Voluptatum et modi sit in et magnam. Nam eos quis facere et ut modi quia.', 21, 90, '2022-12-01 12:37:52', NULL),
	(37, 'Sed Culpa', '@Slovenia', 1, 'https://picsum.photos/140/100?random=33', 'Aut illo ducimus sunt non. Tempora eos occaecati unde qui accusantium quas minus.', 21, 84, '2022-12-01 12:37:52', NULL),
	(38, 'Libero Deserunt', '@Saint Helena', 1, 'https://picsum.photos/140/100?random=34', 'Voluptatem rerum impedit architecto. Accusantium quae sed placeat est maiores mollitia voluptates minima.', 21, 95, '2022-12-01 12:37:52', NULL),
	(39, 'Eveniet', '@Liechtenstein', NULL, 'https://picsum.photos/140/100?random=35', 'Facilis et dolores veritatis id ipsum. Nulla necessitatibus perspiciatis aut.', 21, 3, '2022-12-01 12:37:52', NULL),
	(40, 'Explicabo', '@Cambodia', 1, 'https://picsum.photos/140/100?random=36', 'Qui occaecati quisquam omnis nesciunt. Qui cupiditate esse quia. Aut qui quis et minus vero qui et.', 14, 6, '2022-12-01 12:37:52', NULL),
	(41, 'Ratione', '@Oman', 1, 'https://picsum.photos/140/100?random=37', 'Officiis dolore consectetur et eos labore velit. Debitis consectetur doloremque assumenda sint beatae perferendis. Non adipisci vero et fuga.', 7, 68, '2022-12-01 12:37:52', NULL),
	(42, 'Distinctio', '@El Salvador', 1, 'https://picsum.photos/140/100?random=38', 'Aut sit dignissimos quos pariatur numquam quasi. Atque corrupti molestiae aut beatae non dolores perspiciatis.', 5, 85, '2022-12-01 12:37:52', NULL),
	(43, 'Sed', '@Malaysia', 1, 'https://picsum.photos/140/100?random=39', 'Et qui soluta ut occaecati soluta. Voluptas et eveniet voluptate qui odio harum. Natus cupiditate molestiae maxime quibusdam.', 5, 6, '2022-12-01 12:37:52', NULL),
	(44, 'Accusantium', '@San Marino', 1, 'https://picsum.photos/140/100?random=40', 'Officiis laborum sint distinctio sed. Vero eius deserunt error.', 5, 70, '2022-12-01 12:37:52', NULL),
	(45, 'Tempora', '@Russian Federation', NULL, 'https://picsum.photos/140/100?random=41', 'Et dignissimos porro eos aut. Neque qui perferendis quae officia aliquam sit. Ducimus deleniti facilis modi.', 7, 59, '2022-12-01 12:37:52', NULL),
	(46, 'Maxime', '@Togo', 1, 'https://picsum.photos/140/100?random=42', 'Asperiores eum qui et soluta soluta et. Nihil odio ut ipsa velit. Non quas ut animi.', 5, 51, '2022-12-01 12:37:52', NULL),
	(47, 'Dolorem', '@Kuwait', 1, 'https://picsum.photos/140/100?random=43', 'Ipsam recusandae autem necessitatibus voluptatem magni quia. Error saepe quo fugiat fugit. Id dicta dolor et qui sit autem quasi.', 21, 45, '2022-12-01 12:37:52', NULL),
	(48, 'Repellat', '@Portugal', 1, 'https://picsum.photos/140/100?random=44', 'Non vero amet est et nostrum et. Aut rem repudiandae vero non. Error ducimus reprehenderit sit necessitatibus.', 14, 28, '2022-12-01 12:37:52', NULL),
	(49, 'Nihil Placeat', '@Iceland', 1, 'https://picsum.photos/140/100?random=45', 'Sapiente in illum quia quae labore velit. Beatae molestiae aspernatur qui commodi.', 30, 15, '2022-12-01 12:37:52', NULL),
	(50, 'Veritatis', '@Isle of Man', 1, 'https://picsum.photos/140/100?random=46', 'Praesentium tenetur autem eligendi assumenda ut modi vel. Quis laborum error nemo nulla omnis. Dolor voluptates quisquam cumque veritatis maiores ut quod.', 30, 10, '2022-12-01 12:37:52', NULL),
	(51, 'Quas Quam', '@Saint Pierre and Miquelon', 1, 'https://picsum.photos/140/100?random=47', 'Consequatur eum nobis eveniet consequuntur quis ullam. Veniam dicta repudiandae hic vitae rerum commodi velit. Qui et quia qui nihil.', 21, 85, '2022-12-01 12:37:52', NULL),
	(52, 'Ut Qui', '@Bahamas', 1, 'https://picsum.photos/140/100?random=48', 'Quia illo dignissimos cupiditate deleniti ex eaque consequuntur dignissimos. Fugit sunt vitae eius ea quisquam commodi ipsum sit. Dolore maiores adipisci consequuntur exercitationem autem sint.', 7, 35, '2022-12-01 12:37:52', NULL),
	(53, 'Est Provident', '@Kiribati', 1, 'https://picsum.photos/140/100?random=49', 'Neque consequatur architecto eum porro quia voluptatem. In corrupti recusandae consectetur iusto dolor sit.', 30, 32, '2022-12-01 12:37:52', NULL),
	(54, 'Vitae Molestias', '@Burundi', 1, 'https://picsum.photos/140/100?random=50', 'Ad sint pariatur repellat nobis fugit. Incidunt aut earum et corporis veniam.', 7, 20, '2022-12-01 12:37:52', NULL),
	(55, 'Vel', '@Faroe Islands', 1, 'https://picsum.photos/140/100?random=51', 'Et et ipsum est ut quam. Fuga asperiores et quas voluptatem exercitationem magni. Voluptate et quos qui.', 21, 12, '2022-12-01 12:37:52', NULL),
	(56, 'Aut', '@Liechtenstein', 1, 'https://picsum.photos/140/100?random=52', 'Dolorum quia hic molestiae illum sequi est architecto ipsa. Aut temporibus odit vel nostrum.', 5, 70, '2022-12-01 12:37:52', NULL),
	(57, 'Qui', '@United States Virgin Islands', 1, 'https://picsum.photos/140/100?random=53', 'Dolorum quis officiis aperiam nostrum similique nostrum. Nostrum necessitatibus magnam ipsam aspernatur porro quisquam repellendus. Libero ipsum dignissimos fugiat suscipit.', 21, 61, '2022-12-01 12:37:52', NULL),
	(58, 'Neque', '@Guam', 1, 'https://picsum.photos/140/100?random=54', 'Assumenda totam aperiam voluptate sunt quasi voluptatum molestiae. Laboriosam voluptatem delectus quia facere laboriosam architecto odit. Dolorum veniam rerum ipsam temporibus accusamus.', 21, 27, '2022-12-01 12:37:52', NULL),
	(59, 'Error', '@Paraguay', 1, 'https://picsum.photos/140/100?random=55', 'Itaque laborum soluta sed dolorum ipsa rerum inventore. Praesentium nihil et eligendi et nobis.', 5, 21, '2022-12-01 12:37:52', NULL),
	(60, 'Aliquam', '@Serbia', 1, 'https://picsum.photos/140/100?random=56', 'Ut at exercitationem aut. Assumenda vero sint voluptas aliquid assumenda. Ipsa libero ipsum tenetur fuga.', 30, 48, '2022-12-01 12:37:52', NULL),
	(61, 'Ea', '@Thailand', 1, 'https://picsum.photos/140/100?random=57', 'Animi iste quas vel aut beatae nihil. Pariatur dignissimos unde inventore in rerum quo. Expedita quisquam distinctio labore voluptatibus voluptas.', 14, 84, '2022-12-01 12:37:52', NULL),
	(62, 'Illo', '@Guatemala', NULL, 'https://picsum.photos/140/100?random=58', 'Excepturi aperiam in est natus. Impedit esse soluta tenetur aut id aut.', 21, 58, '2022-12-01 12:37:52', NULL),
	(63, 'Eum', '@Cape Verde', 1, 'https://picsum.photos/140/100?random=59', 'Autem enim aperiam quia et saepe exercitationem cum quia. Amet eveniet et sunt consequatur sit. Ut enim voluptatum sit perferendis dicta.', 30, 7, '2022-12-01 12:37:52', NULL),
	(64, 'Quibusdam Velit', '@Cape Verde', 1, 'https://picsum.photos/140/100?random=60', 'Aut esse maxime aut sequi. Officia consectetur modi quaerat alias.', 30, 85, '2022-12-01 12:37:52', NULL),
	(65, 'Esse Dolorem', '@British Indian Ocean Territory (Chagos Archipelago)', 1, 'https://picsum.photos/140/100?random=61', 'Temporibus nisi sequi magnam consequatur. Et et dolorem minima consequatur aut officiis. Atque distinctio ea similique asperiores corporis quis.', 7, 79, '2022-12-01 12:37:52', NULL),
	(66, 'Dolorem Recusandae', '@Pitcairn Islands', 1, 'https://picsum.photos/140/100?random=62', 'Est quia eum quia non. Sed nihil ipsa enim rerum. Sed reprehenderit quibusdam recusandae cum id nobis maxime.', 7, 26, '2022-12-01 12:37:52', NULL),
	(67, 'Quos Optio', '@Chile', 1, 'https://picsum.photos/140/100?random=63', 'Non itaque voluptatem et nostrum. Ducimus sapiente vero consectetur in fugiat.', 30, 10, '2022-12-01 12:37:52', NULL),
	(68, 'Quia', '@Falkland Islands (Malvinas)', 1, 'https://picsum.photos/140/100?random=64', 'Fugit illum ut tempore sed culpa sed repellat. Nam tenetur nam sint assumenda numquam dolores.', 21, 73, '2022-12-01 12:37:52', NULL),
	(69, 'Repellendus', '@Somalia', 1, 'https://picsum.photos/140/100?random=65', 'Doloribus enim impedit voluptas dolor aut velit aliquam nemo. Dolores quaerat distinctio non aut nihil. Voluptatem id alias deleniti ea ducimus et repellat.', 3, 52, '2022-12-01 12:37:52', NULL),
	(70, 'Fugit Corporis', '@Argentina', 1, 'https://picsum.photos/140/100?random=66', 'Sed a sit est maiores dolorum ducimus et. Odio porro quaerat suscipit iste vel.', 21, 52, '2022-12-01 12:37:52', NULL),
	(71, 'Dignissimos', '@French Polynesia', 1, 'https://picsum.photos/140/100?random=67', 'Corrupti velit ab vero eum quaerat et. Quas quod rerum recusandae neque soluta quibusdam.', 7, 62, '2022-12-01 12:37:52', NULL),
	(72, 'Aspernatur', '@Poland', 1, 'https://picsum.photos/140/100?random=68', 'Vero occaecati assumenda autem omnis. Quidem optio maxime rerum similique reiciendis omnis. Dolores debitis est nisi odio veritatis voluptate.', 3, 69, '2022-12-01 12:37:52', NULL),
	(73, 'Fuga Porro', '@Saudi Arabia', 1, 'https://picsum.photos/140/100?random=69', 'Quas aut sit ea illum ad. Esse suscipit sint illo nostrum.', 21, 62, '2022-12-01 12:37:52', NULL),
	(74, 'At', '@Suriname', 1, 'https://picsum.photos/140/100?random=70', 'Aperiam ut laudantium illum aut quasi debitis odio. Sit libero et ratione dignissimos. Eligendi quibusdam ipsum alias quod ab aspernatur qui sunt.', 14, 27, '2022-12-01 12:37:52', NULL),
	(75, 'Et Hic', '@Iraq', 1, 'https://picsum.photos/140/100?random=71', 'Et aut ut porro. Et amet dolorum error unde quidem voluptas maiores aliquam.', 14, 88, '2022-12-01 12:37:52', NULL),
	(76, 'Nisi Ipsa', '@Djibouti', 1, 'https://picsum.photos/140/100?random=72', 'Rerum est fugit delectus quos voluptas aliquid neque. Quidem magnam voluptatum quis sunt cumque odio.', 14, 55, '2022-12-01 12:37:52', NULL),
	(77, 'Nostrum Sit', '@Greenland', 1, 'https://picsum.photos/140/100?random=73', 'Doloribus maxime odio velit hic autem tenetur. Architecto ea minima voluptatibus facere voluptas fugit.', 5, 70, '2022-12-01 12:37:52', NULL),
	(78, 'Sunt', '@Chile', NULL, 'https://picsum.photos/140/100?random=74', 'Iure aut sed provident hic unde omnis nisi. Deserunt aut dicta ratione consequuntur deleniti.', 5, 38, '2022-12-01 12:37:52', NULL),
	(79, 'Quod', '@Solomon Islands', 1, 'https://picsum.photos/140/100?random=75', 'Est voluptatem aliquam accusantium cupiditate natus debitis laboriosam in. Vero dolore dolorum reprehenderit voluptas repellendus. At ex nemo et aut doloremque id aut.', 7, 81, '2022-12-01 12:37:52', NULL),
	(80, 'Iure', '@Burundi', 1, 'https://picsum.photos/140/100?random=76', 'Ab quis qui sed. Praesentium aut officia omnis.', 30, 96, '2022-12-01 12:37:52', NULL),
	(81, 'Modi', '@Niue', 1, 'https://picsum.photos/140/100?random=77', 'Magnam sit inventore quidem mollitia harum similique animi. Et exercitationem sit dolorem sed nesciunt qui et. Fuga tempore quod omnis aut dolorum non error.', 21, 64, '2022-12-01 12:37:52', NULL),
	(82, 'Animi', '@Spain', 1, 'https://picsum.photos/140/100?random=78', 'Omnis aut nisi perspiciatis officiis esse voluptatum. Est architecto alias consequatur nihil. Et reiciendis culpa doloribus minima porro laudantium.', 7, 99, '2022-12-01 12:37:52', NULL),
	(83, 'Odit', '@Rwanda', 1, 'https://picsum.photos/140/100?random=79', 'Accusantium qui unde fugiat et sint optio. Dolorem totam officiis aspernatur est fugiat dolores doloribus eos.', 14, 36, '2022-12-01 12:37:52', NULL),
	(84, 'Dolorum Consectetur', '@Guadeloupe', 1, 'https://picsum.photos/140/100?random=80', 'Placeat in perspiciatis iste labore fuga non. Et enim ut doloribus porro iste.', 21, 32, '2022-12-01 12:37:52', NULL),
	(85, 'Pariatur', '@Estonia', NULL, 'https://picsum.photos/140/100?random=81', 'Ipsam dicta culpa dolores esse iure et a quas. Reiciendis quis sint qui dignissimos reiciendis in quis.', 7, 52, '2022-12-01 12:37:52', NULL),
	(86, 'Quibusdam', '@India', 1, 'https://picsum.photos/140/100?random=82', 'Qui totam reiciendis aperiam at aut. Earum earum in vel aut aspernatur in. Qui et consequatur iure corrupti et.', 14, 30, '2022-12-01 12:37:52', NULL),
	(87, 'Hic Fugiat', '@Zambia', 1, 'https://picsum.photos/140/100?random=83', 'Illo voluptatem cum magni suscipit ut. Ad aut qui dolor harum ducimus sed. Consectetur excepturi reprehenderit mollitia quae ut et.', 5, 69, '2022-12-01 12:37:52', NULL),
	(88, 'Reiciendis', '@Dominican Republic', 1, 'https://picsum.photos/140/100?random=84', 'Eaque eaque ea nesciunt aut ducimus nam atque. Laborum et labore culpa asperiores minus quod eius eos.', 5, 3, '2022-12-01 12:37:52', NULL),
	(89, 'Voluptatibus Aut', '@Kyrgyz Republic', 1, 'https://picsum.photos/140/100?random=85', 'Iste aut sed iure quas ut vel. Ut blanditiis ea pariatur odit quia. Dolores laborum enim est non et architecto praesentium.', 30, 52, '2022-12-01 12:37:52', NULL),
	(90, 'Eos', '@China', 1, 'https://picsum.photos/140/100?random=86', 'Ut ab deleniti exercitationem aspernatur qui. Consequatur sit in voluptatem inventore occaecati sit.', 7, 62, '2022-12-01 12:37:52', NULL),
	(91, 'Voluptatibus', '@China', 1, 'https://picsum.photos/140/100?random=87', 'Totam ex molestiae doloribus a ut voluptas libero. Et quia a reiciendis quia a labore.', 14, 32, '2022-12-01 12:37:52', NULL),
	(92, 'Quia Perspiciatis', '@Sao Tome and Principe', 1, 'https://picsum.photos/140/100?random=88', 'Sit quaerat quidem quis sit illum non dolore voluptatem. Perferendis voluptatem et cumque illo in vel adipisci. Neque et qui qui quia.', 21, 56, '2022-12-01 12:37:52', NULL),
	(93, 'Sit', '@Zimbabwe', 1, 'https://picsum.photos/140/100?random=89', 'Quo vero itaque placeat voluptates voluptate quisquam labore omnis. Aliquam est quisquam saepe est atque omnis et. Laudantium nihil sit nisi voluptatum sint quis dolorem.', 7, 40, '2022-12-01 12:37:52', NULL),
	(94, 'Velit Aut', '@Anguilla', 1, 'https://picsum.photos/140/100?random=90', 'Nisi aspernatur sint quos expedita. Beatae doloribus soluta fugit nihil ea doloremque adipisci fugiat. Vitae iste consequatur molestiae aliquam ea ullam.', 5, 89, '2022-12-01 12:37:52', NULL),
	(95, 'Dolores Quia', '@Montserrat', 1, 'https://picsum.photos/140/100?random=91', 'Exercitationem ut non dolorem ipsam quos dicta laboriosam. Hic cum in ipsa aut.', 14, 57, '2022-12-01 12:37:52', NULL),
	(96, 'Fugit Earum', '@Puerto Rico', 1, 'https://picsum.photos/140/100?random=92', 'Commodi dignissimos non aliquam quae maiores labore blanditiis. Cum optio repellat atque maiores eveniet ea dolor facilis.', 3, 76, '2022-12-01 12:37:52', NULL),
	(97, 'Voluptates', '@Aruba', 1, 'https://picsum.photos/140/100?random=93', 'Facere vel accusamus deleniti et aperiam autem quos. Ut inventore quis aut dolor voluptas. Dolorum eum corporis provident corporis ab.', 5, 70, '2022-12-01 12:37:52', NULL),
	(98, 'Ut Molestias', '@Tonga', NULL, 'https://picsum.photos/140/100?random=94', 'Natus et dolor iusto non maiores. Sit fugit libero consequatur ipsam facere quia.', 30, 56, '2022-12-01 12:37:52', NULL),
	(99, 'Quae Optio', '@Cayman Islands', 1, 'https://picsum.photos/140/100?random=95', 'Eum dignissimos aut placeat voluptate et. Assumenda veniam dolor omnis soluta deserunt sed et.', 3, 34, '2022-12-01 12:37:52', NULL),
	(100, 'Ut', '@South Africa', 1, 'https://picsum.photos/140/100?random=96', 'Officiis cum nostrum dolor. Commodi consectetur nisi ipsam rem provident. Nesciunt autem sit voluptatibus atque.', 7, 35, '2022-12-01 12:37:52', NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Listage de la structure de la table fox. users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_activate` tinyint(1) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table fox.users : ~2 rows (environ)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `is_activate`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'Pier', 'Pier@fox.com', 1, '$2y$10$jFL4cYxLXYOwYkdtgLAMduKjxh7vQEHRAoM6k.NMFcnyEsLy7nNQ.', '2022-12-01 12:37:52', NULL),
	(2, 'Pol', 'Pol@fox.com', 0, '$2y$10$BZB.XTIgwhKlLmq6jg/luuT4zoYndFcsom7JDqpaqCdVMlCwmdp/m', '2022-12-01 12:37:52', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
