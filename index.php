<?php

// die ('Ready 21');
use App\Tools\Gc7;
use Inphinit\Teeny;
use App\Templating\Template;
use App\Tools\IframeController;
use App\Controllers\ImgController;
use App\Controllers\TestController;
use App\Controllers\UserController;
use App\Controllers\MigrationAndSeedController;

require_once './vendor/autoload.php';
require_once './vendor/inphinit/teeny/vendor/Teeny.php';

if (!session_id()) {
	session_start();
}

if (!array_key_exists('count', $_SESSION)) {
	$_SESSION['count'] = 0;
}

$app = new Teeny();

$user = new UserController();

$app->action('GET', '/', function () use ($user) {
	return $user->index();
});

$app->action('GET', 'show/<id:num>', function ($id) use ($user) {
	return $user->show($id);
});

$app->action('GET', 'register', function () use ($user) {
	return $user->register();
});

$app->action('GET', 'login', function () use ($user) {
	return $user->login();
});
$app->action('GET', 'autoLogin', function () use ($user) {
	return $user->autoLogin();
});

$app->action('GET', 'logout', function () use ($user) {
	return $user->logout();
});

$app->action('GET', 'trash', function () use ($user) {
	return $user->trash();
});

$app->action('POST', 'save', function () use ($user) {
	// Gc7::aff($user);

	return $user->save();
});

$app->action('GET', 'delete/<id:num>', function ($id) use ($user) {
	return $user->delete($id);
});

// die (__FILE__.' - '.__LINE__);

$app->action('GET', 't', function () {
	return (new TestController())->test();
});

$app->action('GET', 'count', function () {
	return (new TestController())->count();
});

$app->action('GET', 'increment', function () {
	return (new TestController())->increment();
});

$app->action('GET', 'name', function () {
	return (new TestController())->name();
});

$app->action('GET', 'api/<img>', function ($url) {
	$img = new ImgController();
	
	return $img->manage($url['img']);
});

$app->action('GET', 'g', function () {

	return (new ImgController())->get100Imgs();
});

$app->action('GET', 'migrations', function () {

	return (new MigrationAndSeedController())->run();
});

$app->handlerCodes([403, 404, 405], function ($code) {
	return (new template())->render('pages/index.twig', ['error' => 'Error ' . $code]);
});

return $app->exec();
