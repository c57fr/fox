<?php
namespace App\Controllers;

use App\Tools\Gc7;

class ImgController extends Controller {
	public function manage($url) {
		$nbTofs = $this->nbTofs();
		// Gc7::affFile($nbTofs);
		// Gc7::affFile($url);

		$tofNumber = (explode('/', $url))[4];
		
		// Gc7::affFile($tofNumber);
		
		$url = str_replace(',', '?', $url);

		ini_set('user_agent', 'Mozilla/4.0 (compatible; MSIE 6.0)');
		$file = './src/assets/img/' . $tofNumber . '.jpg';
		// Gc7::affFile($url);
		
		$getImg = file_get_contents($url);
		file_put_contents($file, $getImg);
		
		if ($nbTofs < 7) {
			Gc7::affFile($nbTofs + 1);
			echo '<script type="text/javascript">';
			echo 'window.location.href = /t';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="2;url=/t" />';
			echo '</noscript>';

			return;
		}
		// Gc7::affFile('Fini: ' . $nbTofs + 1);
	}

	public function nbTofs() {
		// gc7::aff(ROOT);

		$dir = ROOT . '/src/assets/img';

		$out = [];
		foreach (glob($dir . '/*.jpg') as $filename) {
			$p     = pathinfo($filename);
			$out[] = $p['filename'];
		}

		return count($out);
	}

	public function get100Imgs() {
		$nb = $this->nbTofs();
		// while ($nb < 5) {
			// $nb = $this->nbTofs();
			$nb++;
			?>
			<script type="text/javascript">
				let i = 0;
				myInterval = setInterval(
					() => {
							i++
							window.open('t', '_blank')
							console.log(i)
							if (i>=10) clearInterval(myInterval)
						}, 3000)
			</script>
<?php
			// header('location: /t');
			// echo '<script type="text/javascript">';
			// echo 'window.location.href = /t';
			// echo '</script>';
			// echo '<noscript>';
			// echo '<meta http-equiv="refresh" content="2;url=/t" />';
			// echo '</noscript>';
		// }

		return true;
	}
}
?>