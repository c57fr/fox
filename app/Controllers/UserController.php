<?php
namespace App\Controllers;

use App\FlashMessage;
use App\Middlewares\Auth;
use App\Models\ProductModel;
use App\Models\UserModel;
use App\Templating\Template;
use App\Tools\Gc7;
use App\Validators\UserValidator;

class UserController extends Controller {
	protected const FORM = 'pages/userForm.twig';

	public function index(): string {
		
		// gc7::aff($_SESSION);
		
		Auth::getInstance();

		$currentPage ??= 1;

		list($data, $page) = $this->pagination($currentPage);

		return $this->template->render('pages/index.twig', [
			'data' => $data,
			'page' => $page,
		]);
	}

	public function register(): string {
		return $this->getForm();
	}

	public function login() {
		$user['action'] = 'login';

		return $this->getForm($user);
	}

	public function show($id) {
		Auth::getInstance();
		$data = (new ProductModel())->getProduct($id['id']);

		return $this->template->render('pages/show.twig', ['data' => $data]);
	}

	public function autoLogin() {
		$username     = 'Pol';
		$user['username'] = $username;

		Auth::login($user);

		$this->home();
	}

	public function save(): string|Template {
		FlashMessage::getInstance()->clearErrors();

		$user = [
			'action'           => $_POST['action'] ?? null,
			'username'         => $_POST['username'] ?? null,
			'email'            => $_POST['email'] ?? null,
			'password'         => $_POST['password'] ?? null,
			'password_confirm' => $_POST['password_confirm'] ?? null,
		];

		UserValidator::validate($user);

		$errors = FlashMessage::getInstance()->getErrors();

		$data = [
			'user'   => $user,
			'errors' => $errors,
		];

		if (FlashMessage::getInstance()->hasErrors()) {
			return $this->template->render(self::FORM, ['data' => $data]);
		}

		Gc7::aff($user, 'user');
		Gc7::aff($user, 'logger');

		if ('create' == $user['action']) {
			(new UserModel())->create($user);
		}

		Auth::login($user);

		$this->home();
	}

	public function edit(array $user): string {
		$user         = (new UserModel())->getUser($user['id']);
		$user->action = 'update';

		return $this->getForm($user);
	}

	public function delete(array $user): void {
		(new UserModel())->getUser($user['id'])->delete();
		$this->home();
	}

	public function logout(): void {
		Auth::logout();
		$this->home();
	}

	public function trash(): void {
		Auth::trash();
		$this->home();
	}

	public function pagination($page) {
		$perPage = PERPAGE;
		$nbPdts  = (new ProductModel())->count();
		$nbPages = ceil($nbPdts / $perPage);

		$offset = $perPage * ($page - 1);

		$data['page']    = $page;
		$data['perPage'] = $perPage;
		$data['nbPages'] = $nbPages;
		$data['offset']  = $offset;
		$data['nbPdts']  = $nbPdts;

		$pdts = (new ProductModel())->getSome($perPage, $offset);

		return [$pdts, $page];
	}

	private function home() {
		header('location:/');
	}

	private function getForm($user = null) {
		$data['user'] = [
			'action'   => $user['action'] ?? 'create',
			'id'       => $user['id'] ?? null,
			'username' => $user['username'] ?? null,
			'email'    => $user['email'] ?? null,
			'password' => $user['password'] ?? null,
		];

		return $this->template->render(self::FORM, ['data' => $data]);
	}
}
