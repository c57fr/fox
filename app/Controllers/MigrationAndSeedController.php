<?php
namespace App\Controllers;

use App\Tools\Gc7;

class MigrationAndSeedController extends Controller {
	public function run() {
		ob_start();
		define('PATH', 'cmd /c C:\laragon\www\fox\\');

		$cmd1 = 'vendor/bin/phinx.bat rollback';
		$cmd2 = 'vendor/bin/phinx.bat migrate';
		$cmd3 = 'vendor/bin/phinx.bat seed:run';

		exec(PATH . $cmd1, $resCmd1);
		exec(PATH . $cmd2, $resCmd2);
		exec(PATH . $cmd3, $resCmd3);

		ob_end_flush();

		$data = [$resCmd1, $resCmd2, $resCmd3];
		
		// gc7::aff($data);

		return $this->template->render('pages/migrations.twig', [
			'data' => $data,
		]);
	}
}
