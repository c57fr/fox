<?php

$GLOBALS['ROOT_DOCUMENT'] = realpath($_SERVER['DOCUMENT_ROOT']);
define('ROOT', realpath($_SERVER['DOCUMENT_ROOT']));
// define('HTTP', realpath($_SERVER['HTTP_REFERER']));

if (!function_exists('getUri')) {
	function getUri(): string {
		return explode('?', $_SERVER['REQUEST_URI'])[0];
	}
}