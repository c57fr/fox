<?php
namespace App\Validators;

use App\Contracts\Validator;
use App\FlashMessage;
use App\Models\UserModel;
use Respect\Validation\Validator as v;

class UserValidator implements Validator {
	public static function validate(array $user): bool {
		if ('create' != $user['action']) {
			$user = (new UserModel())->getUserByCredentitials($user['username'], $user['password']);
			if (!$user) {
				self::addError('username', 'The username or password, (or both), isn\'t right');

				return false;
			}

			return true;
		}

		$usernameIsValid = self::validateUsername($user['username']);
		$passwordIsValid = self::validatePassword($user['password'], $user['password_confirm']);

		$emailIsValid = ('login' == $user['action']) ? true : self::validateEmail($user['email']);

		if (
			!$usernameIsValid
			|| !$emailIsValid
			|| !$passwordIsValid
		) {
			return false;
		}

		return true;
	}

	protected static function addError(string $field, string $message): void {
		FlashMessage::getInstance()->addError($field, $message);
	}

	private static function validateUsername(mixed $username): bool {
		$usernameValidator = v::alnum(' ', '-')->charset('UTF-8')->length(2, 30);

		$user = (new UserModel())->getUserByUsername($username) ?? null;

		if (!$usernameValidator->validate($username) || $user) {
			self::addError('username', 'The username isn\'t right or already registered');

			return false;
		}

		return true;
	}

	private static function validateEmail(string $email): bool {
		$emailValidator = v::alnum('@', '-', '.')->charset('UTF-8')->length(1, 255);

		$user = (new UserModel())->getUserByEmail($email) ?? null;

		if (!$emailValidator->validate($email) || $user) {
			self::addError('email', 'The email isn\'t right or already registered');

			return false;
		}

		return true;
	}

	private static function validatePassword(string $password, string $password_confirm): bool {
		$passwordValidator = v::alnum(' ', '-', '.')->charset('UTF-8')->length(8, 255);

		if ($password !== $password_confirm || !$passwordValidator->validate($password)) {
			self::addError('password', 'Wrong password or confirmation');

			return false;
		}

		return true;
	}
}
