<?php
namespace App\Middlewares;

class Auth {
	public static $user;

	private static $_instance;

	private function __construct() { // To be not callable
	}

	public static function getInstance(): Auth {
		if (is_null(self::$_instance)) {
			self::$_instance = new Auth();
		}

		return self::$_instance;
	}

	public static function user(): void {
		if (empty($_SESSION['user']) || '' == $_SESSION['user']) {
			header('Location: /');
			exit();
		}
	}

  public static function login(array $user): void {
  	$_SESSION = [
  		'debug' => DEBUG,
  		'user'  => $user,
  	];
  }

	public static function logout(): void {
		unset($_SESSION['user']);
	}

  public static function trash(): void {
  	unset($_SESSION['debug'], $_SESSION['user']);
  }
}
