<?php
namespace App\Models;

use App\Tools\Gc7;

class UserModel extends Model {
	protected static string $table = 'users';

	public function all(): array {
		return \ORM::forTable(self::$table)->orderByDesc('id')->findMany();
	}

	public function getUser(int $id): \ORM|bool {
		return \ORM::forTable(self::$table)->findOne($id);
	}

	public function getUserByUsername(string $username): \ORM|bool {
		return \ORM::forTable(self::$table)->where('username', $username)->findOne();
	}

	public function getUserByCredentitials(string $username, string $password): \ORM|bool {
		$user = \ORM::forTable(self::$table)->where('username', $username)->findOne();

		return (password_verify($password, ($user->password ?? ''))) ? $user : false;
	}

	public function getUserByEmail(string $email): \ORM|bool {
		return \ORM::forTable(self::$table)->where('email', $email)->findOne();
	}

	public function create(array $newUser): bool {
		// echo '<hr>';
		// var_dump($newUser);

		list($n, $e, $p) = [$_POST['username'], 2, 3];
		// echo '<hr>';

		$user               = \ORM::forTable(self::$table)->create();
		$user->username     = $newUser['username'];
		$user->email        = $newUser['email'];
		$user->password     = password_hash($newUser['password'], PASSWORD_DEFAULT);
		$user['created_at'] = date('Y-m-d H:i:s');
		var_dump($user->username);

		return $user->save();
	}

	public function isDataUnic(string $type, string $value): \ORM|int {
		// Gc7::aff($type);
		// Gc7::aff($value);

		return !\ORM::forTable(self::$table)->where($type, $value)->count();
	}
}
