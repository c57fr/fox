export default async () => {
  let writer = document.getElementById('goCount');

  writer.addEventListener('click', async () => {
    let input = document.getElementById('test').value;

    let response = await fetch('/name', {
      method: 'POST',
      body: JSON.stringify({'user': input}),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    let data = await response.json();

    const textElement = document.getElementById('test');
    textElement.innerText = data.data;
  });
}
