<?php

const DEBUG = false;

const APP = [
	'name'     => 'APP NAME',
	'database' => [
		'host'     => 'localhost',
		'user'     => 'root',
		'password' => '',
		'dbname'   => 'fox',
	],
	'templating' => [
		'cache'     => false,
		'templates' => 'src/views',
	],
];

const PERPAGE = 3;
