<?php

/**
 * (ɔ) Online FORMAPRO - GrCOTE7 -2022.
 */

use Phinx\Seed\AbstractSeed;

class UsersSeeder extends AbstractSeed
{
	/**
	 * Run Method.
	 *
	 * Write your database seeder using this method.
	 *
	 * More information on writing seeders is available here:
	 * https://book.cakephp.org/phinx/0/en/seeding.html
	 */
	public function run(): void
	{
		$data = [
			[
				'username'    => 'Pier',
				'email'       => 'Pier@fox.com',
				'is_activate' => true,
				'password'    => password_hash('Pier2022', PASSWORD_DEFAULT),
				'created_at'  => date('Y-m-d H:i:s'),
			], [
				'username'    => 'Pol',
				'email'       => 'Pol@fox.com',
				'is_activate' => false,
				'password'    => password_hash('Pol2022', PASSWORD_DEFAULT),
				'created_at'  => date('Y-m-d H:i:s'),
			],
		];

		$users = $this->table('users');
		$users->truncate();
		$users->insert($data)
			->saveData();
	}
}