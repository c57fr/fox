import liveReload from 'vite-plugin-live-reload'
import {defineConfig} from 'vite';

export default defineConfig ({
  plugins: [
    liveReload(['*/**/*.scss', './*.js', './*/**/*.js', './*.twig', './*/**/*.twig', './*.php', '*/**/*.php'])
  ],
  manifest: true,
  rollupOptions: {
    input: '/src/assets/js/main.js'
  },
})
